import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenterPaypalComponent } from './renter-paypal.component';

describe('RenterPaypalComponent', () => {
  let component: RenterPaypalComponent;
  let fixture: ComponentFixture<RenterPaypalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenterPaypalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenterPaypalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
