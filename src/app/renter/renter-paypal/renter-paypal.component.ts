import { Component, AfterViewChecked } from '@angular/core';
import {Globals} from '../../shared/globals';
import {IProperty} from '../../landloard/properties/IProperty';
import {PropertiesService} from '../../landloard/properties/properties.service';

declare let paypal: any;

@Component({
  selector: 'app-renter-paypal',
  templateUrl: './renter-paypal.component.html',
  styleUrls: ['./renter-paypal.component.css']
})
export class RenterPaypalComponent implements AfterViewChecked {


  addScript: boolean = false;
  paypalLoad: boolean = true;
  finalAmount: number = 0;
  propery: IProperty;

  constructor(private globals: Globals, private propertiesService: PropertiesService){
      this.propertiesService.getDataById(this.globals.user.id).subscribe((property) => {
        this.finalAmount = property.price;
    });
  }


    paypalConfig = {
      env: 'sandbox',
      client: {
        sandbox: 'AVTQHVrjz_-lkTRlJ3XaVHOnPfmr0gXoWY2aESOj0PL64MR5njg8rFpA9aJxaenTf-msB_wWQRTNAjCZ',
        production: 'EMJLTncnYhyJtvuwBFXDqR6oh_ndlle4con41Tsxbc9o8fp-tbaK4bcOKxe6RojjBonoUwzyy1rZKFNt'
      },
      commit: true,
      // is called when the button is clicked
      payment: (data, actions) => {
        // Make a call to the rest api to create the payment
        return actions.payment.create({
          payment: {
            transactions: [
              { amount: { total: this.finalAmount, currency: 'USD' } }
            ]
          }
        });
      },
      // is called when the buyer authorize the payent
      onAuthorize: (data, actions) => {
        return actions.payment.execute().then((payment) => {
          alert("Authorized");
        })
      },
      onError: (err) => {
        alert(err);
      }
    };

    ngAfterViewChecked(): void {
      if (!this.addScript) {
        this.addPaypalScript().then(() => {
          paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn');
          this.paypalLoad = false;
        })
      }
    }

    addPaypalScript() {
      this.addScript = true;
      return new Promise((resolve, reject) => {
        let scripttagElement = document.createElement('script');
        scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
        scripttagElement.onload = resolve;
        document.body.appendChild(scripttagElement);
      })
    }

}
