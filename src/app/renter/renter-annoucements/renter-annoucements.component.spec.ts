import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenterAnnoucementsComponent } from './renter-annoucements.component';

describe('RenterAnnoucementsComponent', () => {
  let component: RenterAnnoucementsComponent;
  let fixture: ComponentFixture<RenterAnnoucementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenterAnnoucementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenterAnnoucementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
