import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenterShowAnnouncementComponent } from './renter-show-announcement.component';

describe('RenterShowAnnouncementComponent', () => {
  let component: RenterShowAnnouncementComponent;
  let fixture: ComponentFixture<RenterShowAnnouncementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenterShowAnnouncementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenterShowAnnouncementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
