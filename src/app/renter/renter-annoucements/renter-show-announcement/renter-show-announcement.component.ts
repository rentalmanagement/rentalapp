import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../../../landloard/properties/properties.service';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import {IUser} from '../../../shared/login/iuser';

@Component({
  selector: 'app-renter-show-announcement',
  templateUrl: './renter-show-announcement.component.html',
  styleUrls: ['./renter-show-announcement.component.css']
})
export class RenterShowAnnouncementComponent implements OnInit {

  private  announcement : any;
  private user: IUser;

  constructor(private propService: PropertiesService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
    this.announcement = this.globals.announcement;
    this.propService.getUserById(this.announcement.property.landlordId).subscribe(p => {
      this.user = p;
    });

  }

}
