import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../../landloard/properties/properties.service';
import {AnnouncementsService} from '../../landloard/announcements/announcemets.service';
import {Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {IAnnouncement} from '../../landloard/announcements/IAnnouncement';

@Component({
  selector: 'app-renter-announcements',
  templateUrl: './renter-annoucements.component.html',
  styleUrls: ['./renter-annoucements.component.css']
})
export class RenterAnnouncementsComponent implements OnInit {
  public announcements = [];
  p: number = 1;

  constructor(private propService: PropertiesService, private annService: AnnouncementsService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
    this.announcements = [];
    this.annService.getAllData().subscribe(data => {

      for(let property of data) {
        console.log(property);
        this.propService.getDataById(property.propertyid).subscribe( p => {
          console.log(p);
          this.announcements.push({"id" : property.id, "title": property.title , "property": p});
        });
      }});
  }
  handleShow(ann: IAnnouncement){
    this.globals.announcement = ann;
    this._router.navigate(['/renter/show-announcement']);
  }

}
