import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenterTaskComponent } from './renter-task.component';

describe('RenterTaskComponent', () => {
  let component: RenterTaskComponent;
  let fixture: ComponentFixture<RenterTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenterTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenterTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
