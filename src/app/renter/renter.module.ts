import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RenterComponent } from './renter.component';
import {RenternavmenuComponent} from './renternavmenu/renternavmenu.component';
import {LayoutModule} from '@angular/cdk/layout';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatOptionModule, MatSelectModule,
  MatSidenavModule,
  MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {ProfileComponent} from '../shared/profile/profile.component';
import {RenterAnnouncementsComponent} from './renter-annoucements/renter-annoucements.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {RenterShowAnnouncementComponent} from './renter-annoucements/renter-show-announcement/renter-show-announcement.component';
import {RenterTaskComponent} from './renter-task/renter-task.component';
import { RenterPaypalComponent } from './renter-paypal/renter-paypal.component';


@NgModule({
  declarations: [RenterComponent, RenternavmenuComponent, ProfileComponent,
    RenterAnnouncementsComponent, RenterShowAnnouncementComponent, RenterTaskComponent, RenterPaypalComponent],
  imports: [
    CommonModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    NgxPaginationModule
  ]
})
export class RenterModule { }
