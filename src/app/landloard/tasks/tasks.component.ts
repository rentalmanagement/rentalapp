import { Component, OnInit } from '@angular/core';
import {Globals} from '../../shared/globals';
import {IUser} from '../../shared/login/iuser';
import {ITask} from '../../shared/task/ITask';
import {TaskService} from '../../shared/task/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  private assignee: string;
  private assigner: string;
  private title: string;
  private description: string;
  private assignees: IUser[];
  private tasks: ITask[];

  constructor(private renterService: TaskService,
              private globals: Globals) {
      this.getAssignees();
      this.loadMyTasks();
   }

  ngOnInit() {
  }

  getAssignees(): void{
    this.renterService.getAssignees()
                  .then((users) => {
                                    this.assignees = users;
                                  });
  }

  deleteButton(index): void{
    $.ajax({
      url: 'http://localhost:8083/taskServices/delete' + '?'
      + $.param({"assignee": this.tasks[index].assignee, "assigner": this.tasks[index].assigner,
                  "title": this.tasks[index].title}),
      type: 'DELETE',
      success: function(){
        console.log('Success');
      },
      error: function(){
        console.log('Error');
      }
    });
    this.tasks.splice(index, 1);
  }

  loadMyTasks(): void{
    this.renterService.getTasksByUsername(this.globals.user.username).then((tasks) => {
      this.tasks = tasks;
    });
  }

  handleAddTask(): void{
    this.assigner = this.globals.user.username;
    this.renterService.addTask(this.assignee,
                              this.assigner,
                              this.title,
                              this.description);
    this.clearInputForm();
    alert("A new task has been assigned!");
  }

  clearInputForm(): void{
    this.assignee = "";
    this.assigner = "";
    this.title = "";
    this.description = "";
  }

}
