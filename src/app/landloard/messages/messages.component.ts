import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';
import {Globals} from '../../shared/globals';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

    
    username  ;             //need to bind username
    groupId = 1;          //need to bind groupId
    userId ;          //need to bind userId
    messagesList = [];
    replyMessage = "";
    groupMembers = ""
    self = false;
    group;

    constructor(private messagesService: MessagesService, private globals: Globals) {

     }

     
    ngOnInit() {

      this.userId = this.globals.user.id;
      this.getUser();
      

    }



    getMessagesForGroup(){

      //Get messages for a specific group 
      this.messagesService.getMessages(this.groupId).then((messages) =>{
        if(messages.length != this.messagesList.length){
        // this.messagesList = messages;


        //for each message recieved
        for(var i = 0; i<messages.length; i++){
          this.self = false; 
          console.log(this.username + " ------- " + messages[i].fromName);
          
          //check if message is from current user
          if(this.username === messages[i].fromName){
            this.self = true;         
          }

          let ok = 1;
          for(var j = 0;j<this.messagesList.length; j++){

            if(this.messagesList[j].text === messages[i].message){
    
              ok = 0; 
              break;    
            }
          }  

          if(ok == 1){
            //add message
            this.messagesList.push({
              "from":messages[i].fromName,
              "text":messages[i].message,
              "self":this.self
            })
          }

        }
        }

      }).catch((error) => {return null;});

    }

    getUsersForGroup(){
            //Get messages for a specific group 
            this.messagesService.getUsers(this.groupId).then((users) =>{

              //for each message recieved
              for(var i = 0; i<users.length-1; i++){
                this.groupMembers += users[i].username + ", "
              }
              this.groupMembers += users[users.length-1].username 
      
            }).catch((error) => {return null;});
      
    }
    
    getGroup(){
      this.messagesService.getGroup(this.groupId).then((group) =>{
        this.group = group;

      }).catch((error) => {return null;});

}


    getUser(){
      this.messagesService.getUser(this.userId).then((user) =>{
        console.log("ADAUGAM " + user.username);
        
        this.username = user.username;
        console.log("AM ADAUGAT " + this.username);

        this.getUsersForGroup() ;
        this.getGroup();
  
              //Get messages for a specific group 
              this.messagesService.getMessages(this.groupId).then((messages) =>{
                

                //for each message recieved
                for(var i = 0; i<messages.length; i++){
                  this.self = false; 
                  console.log(this.username + " ------- " + messages[i].fromName);
                  //check if message is from current user
                  if(this.username === messages[i].fromName){
                    this.self = true;         
                    
                  }
                    //add message
                  this.messagesList.push({
                    "from":messages[i].fromName,
                    "text":messages[i].message,
                    "self":this.self
                  })
                  }
                
                }).catch((error) => {return null;});
  
        setInterval(()=>{ this.getMessagesForGroup() }, 1000)

      }).catch((error) => {return null;});

    }


    reply(){
      if(this.replyMessage){
        var date = new Date();
 
        var timestamp = date.getFullYear().toString() + this.pad2(date.getMonth() + 1) + this.pad2( date.getDate()) + this.pad2( date.getHours() ) + this.pad2( date.getMinutes() ) + this.pad2( date.getSeconds() ) 
      //Get messages for a specific group 
      this.messagesService.saveMessage(timestamp, this.userId, this.username, this.replyMessage, this.groupId).then((messages) =>{
        
      }).catch((error) => {return null;});


        this.messagesList.push({
          "from":"You",
          "text":this.replyMessage,
          "self":true
        })
      }
      this.replyMessage = "";
    }

     pad2(n) { return n < 10 ? '0' + n : n }

}
