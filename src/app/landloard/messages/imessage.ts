export interface IMessage {
    id: number ;
    timestamp: Date ;
    from: number ;
    fromName: string ;
    to: number ;
    subject: string ;
    message: string ;
    group: number ;
  }
  