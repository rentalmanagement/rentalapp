import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { IMessage } from './imessage';
import { IUserDTO } from './iuserdto';
import { UserDTO } from './userdto';
import { IGroup } from './igroup';

@Injectable()
export class MessagesService {
  messageServiceUrl: string = 'http://localhost:8089/messageServices/messages';

  constructor(private http: HttpClient) { }


  public getMessages(groupId: number): Promise<IMessage[]>{
     let headers = new HttpHeaders({ 'Content-type': 'application/json', 'Access-Control-Allow-Origin': '*'});
     let options = {headers: headers};
      let url = this.messageServiceUrl + '/groups/' + groupId;
    return this.http.get<IMessage[]>(url, options)
           .toPromise()
           .then((list) => {
             return list;
           })
           .catch((error) => {return null;});
  }


  public getGroup(groupId: number): Promise<IGroup>{
    let headers = new HttpHeaders({ 'Content-type': 'application/json', 'Access-Control-Allow-Origin': '*'});
    let options = {headers: headers};
     let url = 'http://localhost:8089/messageServices/groups/' + groupId;
   return this.http.get<IGroup>(url, options)
          .toPromise()
          .then((group) => {
            return group;
          })
          .catch((error) => {return null;});
 }
 

 public getUser(userId: number): Promise<UserDTO>{
  let headers = new HttpHeaders({ 'Content-type': 'application/json', 'Access-Control-Allow-Origin': '*'});
  let options = {headers: headers};
   let url = 'http://localhost:8080/users/one/' + userId;
 return this.http.get<UserDTO>(url, options)
        .toPromise()
        .then((group) => {
          return group;
        })
        .catch((error) => {return null;});
}


  public getUsers(groupId: number): Promise<IUserDTO[]>{
    let headers = new HttpHeaders({ 'Content-type': 'application/json', 'Access-Control-Allow-Origin': '*'});
    let options = {headers: headers};
     let url = this.messageServiceUrl + '/groups/' + groupId + '/users';
   return this.http.get<IUserDTO[]>(url, options)
          .toPromise()
          .then((list) => {
            return list;
          })
          .catch((error) => {return null;});
 }


  public saveMessage( timestamp: string, from: number, fromName: string, message: string, group: number ): Promise<IMessage>{
    let headers = new HttpHeaders({ 'Content-type': 'application/json'});
    let options = {headers: headers};
    let body = {'timestamp': timestamp,
    'from': from,
    'fromName': fromName,
    'message': message,
    'group': group 
    };
    console.log(body)
    return this.http.post<IMessage>(this.messageServiceUrl, body, options)
          .toPromise()
          .then((message) => {
            return message;
          })
          .catch((error) => {return null;});
  }

}
