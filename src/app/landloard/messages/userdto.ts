export interface UserDTO {
    id : number;
    username: string;
    phoneNumber: string;
    emailAddress: string;
    userType: string;

  }
