import { Component, OnInit } from '@angular/core';
import {PropertiesService} from './properties.service';
import {Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {IProperty} from './IProperty';


@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {
  public records = [] ;

  constructor(private propService: PropertiesService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
    this.propService.getData(this.globals.user.id).subscribe(data => this.records = data);
  }
  handleDelete(id: number) {
    this.propService.delete(id);

    let i = -1;
    for(let property of this.records) {
      if(property.id === id)
        i = this.records.indexOf(property);
    }

    if(i !== -1)
      this.records.splice(i,1);
  }

  handleShow(record: IProperty) {
    this.globals.property = record;
    this._router.navigate(['/landlord/show-property']);

  }
  handleUpdate(record: IProperty){
    this.globals.property = record;
    this._router.navigate(['/landlord/update-property']);
  }


}
