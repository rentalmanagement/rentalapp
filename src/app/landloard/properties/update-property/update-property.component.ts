import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../properties.service';
import {AlertService} from '../../../shared/alert-service.service';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import * as $ from "jquery";

@Component({
  selector: 'app-update-property',
  templateUrl: './update-property.component.html',
  styleUrls: ['./update-property.component.css']
})
export class UpdatePropertyComponent implements OnInit {
  private propertyId: number;
  private addressId: number;
  private address: string;
  private county: string;
  private city: string;
  private numberOfRooms: number;
  private numberOfBathrooms: number;
  private areaSize: number;
  private price: number;
  private description: string;

  constructor(private propertyService: PropertiesService, private alertService: AlertService, private _router: Router, private globals: Globals) {
  }

  ngOnInit() {
    this.propertyId = this.globals.property.id;
    this.addressId = this.globals.property.address.id;
    this.address = this.globals.property.address.address;
    this.county = this.globals.property.address.county;
    this.city = this.globals.property.address.county;
    this.numberOfBathrooms = this.globals.property.numberOfBathrooms;
    this.numberOfRooms = this.globals.property.numberOfRooms;
    this.areaSize = this.globals.property.areaSize;
    this.price = this.globals.property.price;
    this.description = this.globals.property.description;


    $('.input').each(function () {
      $(this).on('blur', function () {
        if ((<string>$(this).val()).trim() != '') {
          $(this).addClass('has-val');
        } else {
          $(this).removeClass('has-val');
        }
      });
    });
  }

  handleUpdate(): void {
    this.propertyService.update(this.propertyId, this.addressId,this.address, this.county, this.city, this.numberOfRooms , this.numberOfBathrooms, this.areaSize , this.price, this.description, this.globals.user.id).then((result) => {
      console.log(result);
      if (result != null) {
        this.alertService.success('The property has been updated! You\'ll be redirected back to Properties page!');
        setTimeout(() => {
          this._router.navigate(['/landlord/properties']);
        }, 1000);
      } else {
        this.alertService.error("Some problem occured! Please try again!");
      }
    });
  }

}
