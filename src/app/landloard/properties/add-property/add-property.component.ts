import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../../shared/alert-service.service';
import {Router} from '@angular/router';
import * as $ from "jquery";
import {PropertiesService} from '../properties.service';
import {Globals} from '../../../shared/globals';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {
  private address: string;
  private county: string;
  private city: string;
  private numberOfRooms: number;
  private numberOfBathrooms: number;
  private areaSize: number;
  private price: number;
  private description: string;

  constructor(private propertyService: PropertiesService, private alertService: AlertService, private _router: Router, private globals: Globals) {
  }

  ngOnInit() {
    $('.input').each(function () {
      $(this).on('blur', function () {
        if ((<string>$(this).val()).trim() != '') {
          $(this).addClass('has-val');
        } else {
          $(this).removeClass('has-val');
        }
      });
    });
  }

  handleAdd(): void {
    this.propertyService.save(this.address, this.county, this.city, this.numberOfRooms , this.numberOfBathrooms, this.areaSize , this.price, this.description, this.globals.user.id).then((result) => {
      console.log(result);
      if (result != null) {
        this.alertService.success('The property has been created! You\'ll be redirected back to Properties page!');
        setTimeout(() => {
            this._router.navigate(['/landlord/properties']);
        }, 1000);
      } else {
        this.alertService.error("Some problem occured! Please try again!");
      }
    });
  }

}
