export interface IAddress {
  id: number;
  address: string;
  county: string;
  city: string;
}
