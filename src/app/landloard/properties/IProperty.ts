import {IAddress} from './IAddress';

export interface IProperty {
  id: number;
  address: IAddress;
  numberOfRooms: number;
  numberOfBathrooms: number;
  areaSize: number;
  price: number;
  description: string;

}
