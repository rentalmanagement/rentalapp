import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IProperty} from './IProperty';
import {Observable} from 'rxjs';
import {IUser} from '../../shared/login/iuser';
import {IAnnouncement} from '../announcements/IAnnouncement';
import {IRenterProperty} from '../announcements/show-property/IRenterProperty';


@Injectable()
export class PropertiesService {
  servicePath = 'http://localhost:8081/propertyServices/properties/';
  renterPath = 'http://localhost:8081/propertyServices/renter/';
  userPath = 'http://localhost:8080/users';
  constructor(private http: HttpClient) {}

  getData(id: number): Observable<IProperty[]> {
    return this.http.get<IProperty[]>(this.servicePath + '?landlord=' + id);
  }
  getDataById(id: number): Observable<IProperty> {
    return this.http.get<IProperty>(this.servicePath + id);
  }

  public save(address: string, county: string, city: string, numberOfRooms: number , numberOfBathrooms: number, areaSize: number , price: number, description: string , landlordId: number): Promise<IProperty> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.post<IProperty>(this.servicePath , {'address': {'address': address,
        'county': county,
        'city': city
      },
      'numberOfRooms': numberOfRooms,
      'numberOfBathrooms': numberOfBathrooms,
      'areaSize': areaSize,
      'price': price,
      'description': description,
      'landlordId' : landlordId
    }, options)
      .toPromise()
      .then((property) => {
        return property;
      })
      .catch((error) => {return null;});
  }
  public update(propertyId:number, addressId: number, address: string, county: string, city: string, numberOfRooms: number , numberOfBathrooms: number, areaSize: number , price: number, description: string , landlordId: number): Promise<IProperty> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.put<IProperty>(this.servicePath , {'id': propertyId, 'address': {'id': addressId, 'address': address,
        'county': county,
        'city': city
      },
      'numberOfRooms': numberOfRooms,
      'numberOfBathrooms': numberOfBathrooms,
      'areaSize': areaSize,
      'price': price,
      'description': description,
      'landlordId' : landlordId
    }, options)
      .toPromise()
      .then((property) => {
        return property;
      })
      .catch((error) => {return null;});
  }


  public saveRenterProperty(renterid: number , propertyid: number): Promise<IRenterProperty> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.post<IUser>(this.renterPath , {'renterid': renterid,
      'propertyid': propertyid
    }, options)
      .toPromise()
      .then((p) => {
        return p;
      })
      .catch((error) => {return null;});
  }

  public delete(id: number) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.delete(this.servicePath + id, options).subscribe((ok) => {console.log(ok); });
  }

  public getUsersByPropertyId(id: number): Observable<IRenterProperty[]> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.get<IRenterProperty[]>(this.renterPath + 'property/' + id, options);
  }

  public getUserById(id: number): Observable<IUser> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.get<IUser>(this.userPath + '/one/' + id, options);
  }
  public getUserByUsername(username: string): Observable<IUser> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.get<IUser>(this.userPath + '?username=' + username, options);
  }
  public deleteRenterProperty(id: number) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
     this.http.delete(this.renterPath + id, options).subscribe((ok) => {console.log(ok); });
  }
}
