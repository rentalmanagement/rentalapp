import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandloardComponent} from './landloard.component';
import {NavmenuComponent} from './navmenu/navmenu.component';
import {LayoutModule} from '@angular/cdk/layout';
import {
  MatButtonModule,
  MatCardModule, MatFormFieldModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule, MatOptionModule, MatSelectModule
} from '@angular/material';
import {PropertiesComponent} from './properties/properties.component';
import {AnnouncementsComponent} from './announcements/announcements.component';
import {MessagesComponent} from './messages/messages.component';
import {PaymentsComponent} from './payments/payments.component';
import {RouterModule} from '@angular/router';
import {AddPropertyComponent} from './properties/add-property/add-property.component';
import {FormsModule} from '@angular/forms';
import { MessagesService } from './messages/messages.service';
import {ShowPropertyComponent} from './announcements/show-property/show-property.component';
import {AddAnnouncementComponent} from './announcements/add-announcement/add-announcement.component';
import {ShowAnnouncementComponent} from './announcements/show-announcement/show-announcement.component';
import {UpdatePropertyComponent} from './properties/update-property/update-property.component';
import {UpdateAnnouncementComponent} from './announcements/update-announcement/update-announcement.component';
import { TasksComponent } from './tasks/tasks.component';


@NgModule({
  declarations: [LandloardComponent,
    NavmenuComponent,
    PropertiesComponent,
    AnnouncementsComponent,
    MessagesComponent,
    PaymentsComponent,
    AddPropertyComponent,
    AddAnnouncementComponent,
    ShowPropertyComponent,
    ShowAnnouncementComponent,
    UpdatePropertyComponent,
    UpdateAnnouncementComponent,
    TasksComponent,
      ],
  imports: [
    CommonModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    MatCardModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,

  ],
  providers: [MessagesService]
})
export class LandloardModule {
}
