export interface IPayment {
  id: number;
  userId: number;
  landlordId: number;
  amount: number;
}
