import { Component, OnInit } from '@angular/core';
import {PaymentsService} from './payments.service';
import {PropertiesService} from '../properties/properties.service';
import {Router} from '@angular/router';
import {Globals} from '../../shared/globals';
import {LoginService} from '../../shared/login/login.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class PaymentsComponent implements OnInit {
  public payments = [];


  constructor(private paymentService: PaymentsService , private propertyService: PropertiesService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
    this.paymentService.getData(this.globals.user.id).subscribe(data => {
      console.log(data);
      for(let d of data) {
        this.propertyService.getUserById(d.userId).subscribe( p => {
          console.log(p);
          this.payments.push({"user" : p.username, "amount": d.amount});
        });
      }
    });
  }

}
