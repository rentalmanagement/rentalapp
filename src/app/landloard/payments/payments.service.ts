import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from '../../shared/login/iuser';
import {IAnnouncement} from '../announcements/IAnnouncement';
import {IRenterProperty} from '../announcements/show-property/IRenterProperty';
import {IPayment} from './ipayment';


@Injectable()
export class PaymentsService {
  servicePath = 'http://localhost:8082/payments';

  constructor(private http: HttpClient) {}

  getData(id: number): Observable<IPayment[]> {
    return this.http.get<IPayment[]>(this.servicePath + '?landlordId=' + id);
  }

}
