import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from '../../shared/login/iuser';
import {IAnnouncement} from './IAnnouncement';
import {IProperty} from '../properties/IProperty';


@Injectable()
export class AnnouncementsService {
  servicePath = 'http://localhost:8081/propertyServices/announcements';
  constructor(private http: HttpClient) {}

  getData(id: number): Observable<IAnnouncement[]> {
    return this.http.get<IAnnouncement[]>(this.servicePath + '?userid=' + id );
  }

  getAllData(): Observable<IAnnouncement[]> {
    return this.http.get<IAnnouncement[]>(this.servicePath + '/all' );
  }

  public save(title: string, propertyid: number, userid: number): Promise<IAnnouncement> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.post<IUser>(this.servicePath , {'propertyid': propertyid,
      'userid': userid,
      'title': title
    }, options)
      .toPromise()
      .then((announcement) => {
        return announcement;
      })
      .catch((error) => {return null;});
  }
  public delete(id: number) {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.delete(this.servicePath + '/' + id, options).subscribe((ok) => {console.log(ok); });
  }

  public update(id:any, title: any, propertyid: any, userid: any): Promise<IAnnouncement> {
    const headers = new HttpHeaders({ 'Content-type': 'application/json'});
    const options = {headers: headers};
    return this.http.put<IAnnouncement>(this.servicePath , {
      'id': id,
      'propertyid': propertyid,
      'userid': userid,
      'title': title
    }, options)
      .toPromise()
      .then((announcement) => {
        return announcement;
      })
      .catch((error) => {return null;});
  }
}
