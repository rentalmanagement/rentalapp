import {IProperty} from '../properties/IProperty';
import {IUser} from '../../shared/login/iuser';


export interface IAnnouncement {
  id: number;
  title: string;
  propertyid: number;
  userid: number;
}
