import {Component, OnInit} from '@angular/core';
import {PropertiesService} from '../../properties/properties.service';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import {IProperty} from '../../properties/IProperty';
import {IUser} from '../../../shared/login/iuser';

@Component({
  selector: 'app-show-property',
  templateUrl: './show-property.component.html',
  styleUrls: ['./show-property.component.css']
})
export class ShowPropertyComponent implements OnInit {
  property: IProperty;
  renters = [];
  users = [];
  selectedUser: IUser;
  username: string;

  constructor(private propService: PropertiesService, private _router: Router, private globals: Globals) {
  }

  ngOnInit() {
    this.users = [];
    this.renters = [];

    this.property = this.globals.property;
    this.propService.getUsersByPropertyId(this.globals.property.id).subscribe(data => {
      this.renters = data;
      for (let renter of data) {
        this.propService.getUserById(renter.renterid).subscribe(p => {
          this.users.push({'id': p.id, 'username': p.username, 'phoneNumber': p.phoneNumber, 'emailAddress': p.emailAddress});
        });
      }
    });
  }

  public handleRemove() {
    let id = -1;
    let i = 0;
    let pos = -1;
    for (i = 0; i < this.renters.length; i++) {
      if (this.renters[i].renterid === this.selectedUser.id) {
        id = this.renters[i].id;
      }
    }
    if (id !== -1) {
      this.propService.deleteRenterProperty(id);
    }

    for (i = 0; i < this.users.length; i++) {
      if (this.users[i].id === this.selectedUser.id) {
        pos = i;
      }
    }
    if (pos !== -1) {
      this.users.splice(pos, 1);
    }
  }

  public handleAdd() {
    this.propService.getUserByUsername(this.username).subscribe(p => {
      const renterid = p.id;
      this.propService.saveRenterProperty(renterid, this.property.id).then((result) => {
        this.renters.push(result);
        this.users.push(p);
        // console.log(result);
        if (result != null) {
        } else {
         console.log('error');
        }
      });
    });
  }
}
