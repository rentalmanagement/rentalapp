import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../properties/properties.service';
import {Router} from '@angular/router';
import {AnnouncementsService} from './announcemets.service';
import {Globals} from '../../shared/globals';
import {IProperty} from '../properties/IProperty';
import {IAnnouncement} from './IAnnouncement';


@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit {
  public records = [] ;
  public announcements = [];

  constructor(private propService: PropertiesService, private annService: AnnouncementsService, private _router: Router, private globals: Globals) {
    this._router.onSameUrlNavigation = 'reload';
  }

  ngOnInit() {
    this.announcements = [];
    this.records = [];
    console.log(this.globals.user.id);
    this.annService.getData(this.globals.user.id).subscribe(data => {this.records = data;
      for(let property of data) {
        this.propService.getDataById(property.propertyid).subscribe( p => {
          this.announcements.push({"id" : property.id, "title": property.title , "property": p});
        });
      }});
  }
  handleDelete(id: number) {
    this.annService.delete(id);
    let i = -1;
    for (let announcement of this.announcements) {
      if (announcement.id === id) {
        i = this.announcements.indexOf(announcement);
      }
    }

    if (i !== -1) {
      this.announcements.splice(i, 1);
    }

  }

  handleShow(ann: IAnnouncement){
    this.globals.announcement = ann;
    this._router.navigate(['/landlord/show-announcement']);
  }

  handleUpdate(announcement: IAnnouncement){
    this.globals.announcement = announcement;
    this._router.navigate(['/landlord/update-announcement']);

  }

}
