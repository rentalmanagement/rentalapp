import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../../properties/properties.service';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import {IProperty} from '../../properties/IProperty';
import {tick} from '@angular/core/testing';
import {AnnouncementsService} from '../announcemets.service';
import {AlertService} from '../../../shared/alert-service.service';

@Component({
  selector: 'app-add-announcement',
  templateUrl: './add-announcement.component.html',
  styleUrls: ['./add-announcement.component.css']
})
export class AddAnnouncementComponent implements OnInit {
  public records = [] ;
  public property: IProperty;
  public title: string;

  constructor(private propService: PropertiesService, private annService: AnnouncementsService, private alertService: AlertService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
    this.propService.getData(this.globals.user.id).subscribe(data => this.records = data);
  }

  handleAdd(): void {
    this.annService.save(this.title, this.property.id, this.globals.user.id).then((result) => {
     // console.log(result);
      if (result != null) {
        this.alertService.success('The announcement has been created! You\'ll be redirected back to Announcements page!');
        setTimeout(() => {
          this._router.navigate(['/landlord/announcements']);
        }, 1000);
      } else {
        this.alertService.error("Some problem occured! Please try again!");
      }
    });
  }

}
