import { Component, OnInit } from '@angular/core';
import {PropertiesService} from '../../properties/properties.service';
import {AlertService} from '../../../shared/alert-service.service';
import {Router} from '@angular/router';
import {Globals} from '../../../shared/globals';
import * as $ from "jquery";
import {AnnouncementsService} from '../announcemets.service';

@Component({
  selector: 'app-update-announcement',
  templateUrl: './update-announcement.component.html',
  styleUrls: ['./update-announcement.component.css']
})
export class UpdateAnnouncementComponent implements OnInit {
 private title: string;

  constructor(private annService: AnnouncementsService, private alertService: AlertService, private _router: Router, private globals: Globals) {}

  ngOnInit() {

    $('.input').each(function () {
      $(this).on('blur', function () {
        if ((<string>$(this).val()).trim() != '') {
          $(this).addClass('has-val');
        } else {
          $(this).removeClass('has-val');
        }
      });
    });
  }

  handleUpdate(): void {
    this.annService.update(this.globals.announcement.id, this.title, this.globals.announcement.property.id, this.globals.announcement.property.landlordId).then((result) => {
      console.log(result);
      if (result != null) {
        this.alertService.success('The announcement has been updated! You\'ll be redirected back to Announcements page!');
        setTimeout(() => {
          this._router.navigate(['/landlord/announcements']);
        }, 1000);
      } else {
        this.alertService.error("Some problem occured! Please try again!");
      }
    });

  }

}
