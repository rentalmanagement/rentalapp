import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import {IProperty} from '../../properties/IProperty';
import {PropertiesService} from '../../properties/properties.service';
import {Globals} from '../../../shared/globals';
import {IAnnouncement} from '../IAnnouncement';
import {IUser} from '../../../shared/login/iuser';

@Component({
  selector: 'app-show-announcement',
  templateUrl: './show-announcement.component.html',
  styleUrls: ['./show-announcement.component.css']
})
export class ShowAnnouncementComponent implements OnInit {
  private  announcement : any;

  constructor(private propService: PropertiesService, private _router: Router, private globals: Globals) { }

  ngOnInit() {
   this.announcement = this.globals.announcement;

  }

}
