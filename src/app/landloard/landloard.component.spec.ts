import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandloardComponent } from './landloard.component';

describe('LandloardComponent', () => {
  let component: LandloardComponent;
  let fixture: ComponentFixture<LandloardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandloardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandloardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
