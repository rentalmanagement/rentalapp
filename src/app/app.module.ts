import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AlertMessageModule} from './shared/alert-message.module';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './shared/login/login.component';
// import { SigninComponent } from './login/signin.component';
import {LoginModule} from './shared/login/login.module';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {PropertiesService} from './landloard/properties/properties.service';
import {AnnouncementsService} from './landloard/announcements/announcemets.service';
import {Globals} from './shared/globals';
import { RenternavmenuComponent } from './renter/renternavmenu/renternavmenu.component';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { ProfileComponent } from './shared/profile/profile.component';
import { ShowAnnouncementComponent } from './landloard/announcements/show-announcement/show-announcement.component';
import { RenterShowAnnouncementComponent } from './renter/renter-annoucements/renter-show-announcement/renter-show-announcement.component';
import { UpdatePropertyComponent } from './landloard/properties/update-property/update-property.component';
import { UpdateAnnouncementComponent } from './landloard/announcements/update-announcement/update-announcement.component';
import {FormsModule} from '@angular/forms';
import {PaymentsService} from './landloard/payments/payments.service';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    LoginModule,
    BrowserAnimationsModule,
    AlertMessageModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: 'login', component: LoginComponent},
      {path: '', redirectTo: 'login', pathMatch: 'full'}
    ]),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  exports: [RouterModule],
  providers: [PropertiesService , AnnouncementsService, Globals , PaymentsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
