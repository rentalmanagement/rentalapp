import { Injectable } from '@angular/core';
import {IUser} from './login/iuser';
import {IProperty} from '../landloard/properties/IProperty';
import {IAnnouncement} from '../landloard/announcements/IAnnouncement';

@Injectable()
export class Globals {
  user: IUser;
  property: IProperty;
  announcement: any;

}
