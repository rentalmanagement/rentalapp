import { Component, OnInit } from '@angular/core';
import {LoginService} from '../login/login.service';
import {AlertService} from '../alert-service.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {Globals} from '../globals';
import { tick } from '@angular/core/src/render3';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit
 {

  private username: string;
  private phoneNumber: string;
  private email: string;
  private password: string;
  private currentPassword: string;
  constructor(private loginService: LoginService, private alertService: AlertService, private _router: Router , private globals: Globals)
   {
   }

  ngOnInit() {
    this.username = this.globals.user.username;
    this.phoneNumber = this.globals.user.phoneNumber;
    this.email = this.globals.user.emailAddress;
  }

  handleUpdate(): void {
    this.loginService.login(this.globals.user.username,this.currentPassword).then((res)=>
    {
      if(res != null)
      {
        this.loginService.update(this.username, this.phoneNumber, this.email, this.password , this.globals.user.id,this.globals.user.userType).then((result) =>
        {
          console.log(this.globals.user.id);
          console.log(result);
          if (result != null) {
            this.alertService.success('User was updated!!!');
          }
          else
           {
            this.alertService.error("Some problem occured! Please try again!");
          }

          this.globals.user.username = this.username;
          this.globals.user.emailAddress= this.email;
          this.globals.user.phoneNumber= this.phoneNumber;
        });
      }
      else this.alertService.error('Error!');
    });
  }
}
