export interface IUser {
  id: number;
  username: string;
  phoneNumber: string;
  emailAddress: string;
  userType: string;
}
