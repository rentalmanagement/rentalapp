import { Injectable } from '@angular/core';
import { IUser } from './iuser';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loginServiceUrl: string = 'http://localhost:8080/users';

  constructor(private http: HttpClient) { }

  public login(username: string, password: string): Promise<IUser>{
     let headers = new HttpHeaders({ 'Content-type': 'application/json',
   'Access-Control-Allow-Origin': '*'});
     let options = {headers: headers};
    // return this.http.post<IUser>(this.loginServiceUrl, {params:{'username': username, 'password': password }}, options)
    return this.http.post<IUser>(this.loginServiceUrl + '/login', {'username': username, 'password': password }, options)
           .toPromise()
           .then((user) => {
             return user;
           })
           .catch((error) => {alert(error);return null;});
  }

  public singin(username: string, phoneNumber: string, emailAddress: string, landloard: boolean, password: string): Promise<IUser>{
    let headers = new HttpHeaders({ 'Content-type': 'application/json'});
    let options = {headers: headers};
    return this.http.post<IUser>(this.loginServiceUrl + '/save', {"user": {'username': username,
                                                                'phoneNumber': phoneNumber,
                                                                'emailAddress': emailAddress,
                                                                'userType': landloard == true ? 'LANDLORD' : 'RENTER'
                                                              },
                                                                'password': password }, options)
          .toPromise()
          .then((user) => {
            return user;
          })
          .catch((error) => {return null;});
  }
  public update(username: string, phoneNumber: string, email: string, password: string, id: number,userType: string)
  {
    let headers = new HttpHeaders({ 'Content-type': 'application/json'});
    let options = {headers: headers};
    return this.http.put<IUser>(this.loginServiceUrl + '/update', {"user": {
                                                                'id': id,
                                                                'username': username,
                                                                'phoneNumber': phoneNumber,
                                                                'emailAddress': email,
                                                                'userType': userType
                                                              },
                                                                'password': password }, options)
          .toPromise()
          .then((user) => {
            return user;
          })
          .catch((error) => {return null;});
  }
}
