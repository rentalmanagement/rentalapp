import {Component, OnInit} from '@angular/core';
import {LoginService} from './login.service';
import {AlertService} from '../alert-service.service';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {Globals} from '../globals';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private username: string;
  private password: string;

  constructor(private loginService: LoginService, private alertService: AlertService, private _router: Router , private globals: Globals) {
  }

  ngOnInit() {
    $('.input').each(function () {
      $(this).on('blur', function () {
        if ((<string>$(this).val()).trim() != '') {
          $(this).addClass('has-val');
        } else {
          $(this).removeClass('has-val');
        }
      });
    });
  }

  handleLogin(): void {
    this.loginService.login(this.username, this.password).then((result) => {
      console.log(result);
      if (result != null) {
        this.alertService.success('You have been successfully logged in!');
        this.globals.user = result;
        setTimeout(() => {
          if (result.userType == 'LANDLORD') {
            this._router.navigate(['/landlord']);
          } else {
            this._router.navigate(['/renter']);
          }
        }, 1000);
      } else {
        this.alertService.error("There are some problems with your credentials. Please try again!")
      }
    });
  }


}
