import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './login.component';
import {SigninComponent} from './signin.component';
import {LandloardComponent} from '../../landloard/landloard.component';
import {RenterComponent} from '../../renter/renter.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LandloardModule} from '../../landloard/landloard.module';
import {RenterModule} from '../../renter/renter.module';
import {PropertiesComponent} from '../../landloard/properties/properties.component';
import {AnnouncementsComponent} from '../../landloard/announcements/announcements.component';
import {PaymentsComponent} from '../../landloard/payments/payments.component';
import {MessagesComponent} from '../../landloard/messages/messages.component';
import {AddPropertyComponent} from '../../landloard/properties/add-property/add-property.component';
import {AddAnnouncementComponent} from '../../landloard/announcements/add-announcement/add-announcement.component';
import {ProfileComponent} from '../profile/profile.component';
import {RenterAnnouncementsComponent} from '../../renter/renter-annoucements/renter-annoucements.component';
import {ShowPropertyComponent} from '../../landloard/announcements/show-property/show-property.component';
import {ShowAnnouncementComponent} from '../../landloard/announcements/show-announcement/show-announcement.component';
import {RenterShowAnnouncementComponent} from '../../renter/renter-annoucements/renter-show-announcement/renter-show-announcement.component';
import {UpdatePropertyComponent} from '../../landloard/properties/update-property/update-property.component';
import {UpdateAnnouncementComponent} from '../../landloard/announcements/update-announcement/update-announcement.component';
import {RenterTaskComponent} from '../../renter/renter-task/renter-task.component';
import {TasksComponent} from '../../landloard/tasks/tasks.component';
import {RenterPaypalComponent} from '../../renter/renter-paypal/renter-paypal.component';

@NgModule({
  declarations: [LoginComponent, SigninComponent ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    LandloardModule,
    RenterModule,
    RouterModule,
    RouterModule.forChild([
      {path: 'sign-in', component: SigninComponent},
      {
        path: 'landlord', component: LandloardComponent, children: [
          {path: 'profile', component: ProfileComponent},
          {path: 'properties', component: PropertiesComponent},
          {path: 'announcements', component: AnnouncementsComponent},
          {path: 'payments', component: PaymentsComponent},
          {path: 'messages', component: MessagesComponent},
          {path: 'add-property', component: AddPropertyComponent},
          {path: 'add-announcement', component: AddAnnouncementComponent},
          {path: 'show-property', component: ShowPropertyComponent},
          {path: 'show-announcement' , component : ShowAnnouncementComponent},
          {path: 'update-property', component: UpdatePropertyComponent},
          {path: 'update-announcement', component: UpdateAnnouncementComponent},
          {path: 'tasks', component: TasksComponent},
        ]
      },
      {path: 'renter', component: RenterComponent, children: [
          {path: 'profile', component: ProfileComponent},
          {path: 'announcements', component: RenterAnnouncementsComponent},
          {path: 'messages', component: MessagesComponent},
          {path: 'show-announcement' , component: RenterShowAnnouncementComponent},
          {path: 'tasks', component: RenterTaskComponent},
          {path: 'payments', component: RenterPaypalComponent},
        ]}
    ])
  ]
})
export class LoginModule {
}
