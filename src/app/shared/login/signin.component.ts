import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from './login.service';
import {AlertService} from '../alert-service.service';
import * as $ from 'jquery';
import {Globals} from '../globals';
@Component({
  templateUrl: './signin.component.html',
  styleUrls: ['./login.component.css']
})
export class SigninComponent implements OnInit {
  private username: string;
  private password: string;
  private repeatPassword: string;
  private phoneNumber: string;
  private emailAddress: string;
  private userType: boolean;

  constructor(private loginService: LoginService, private alertService: AlertService, private _router: Router, private globals: Globals) {
  }

  ngOnInit() {
    $('.input').each(function () {
      $(this).on('blur', function () {
        if ((<string>$(this).val()).trim() != '') {
          $(this).addClass('has-val');
        } else {
          $(this).removeClass('has-val');
        }
      });
    });
  }

  handleSignIn(): void {
    this.loginService.singin(this.username, this.phoneNumber, this.emailAddress, this.userType, this.password).then((result) => {
      console.log(result);
      if (result != null) {
        this.alertService.success('The account has been created! You\'ll be redirected to homepage!');
        this.globals.user = result;
        setTimeout(() => {
          if (result.userType == 'LANDLORD') {
            this._router.navigate(['/landlord']);
          } else {
            this._router.navigate(['/renter']);
          }
        }, 1000);
      } else {
        this.alertService.error("Some problem occured! Please try again!");
      }
    });
  }
}
