import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import {IUser} from '../login/iuser';
import {Globals} from '../globals';
import {ITask} from './ITask';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  taskServiceUrl: string = 'http://localhost:8083/taskServices';
  userServiceUrl: string = 'http://localhost:8080/users';

  constructor(private http: HttpClient, private globals: Globals) { }

  public addTask(assignee: string,
                assigner: string,
                title: string,
                description: string){
                  let headers = new HttpHeaders({ 'Content-type': 'application/json'});
                  let options = {headers: headers};
                      return this.http.post(this.taskServiceUrl + '/add',
                    {"assignee": assignee,
                      "assigner": assigner,
                      "title": title,
                      "description": description}, options)
                      .toPromise()
                      .catch((error)=> {return null;})
                }

   public getAssignees(): Promise<IUser[]>{
     let headers = new HttpHeaders({ 'Content-type': 'application/json'});
     let options = {headers: headers};
     return this.http.get(this.userServiceUrl + '/all', options)
            .toPromise()
            .then((users) => {
              return users;
            })
            .catch((error) => {return null;});

   }

   public getTasksByUsername(username: string): Promise<ITask[]>{

     return this.http.get(this.taskServiceUrl + '/by-Assignee', {
       params: {
         assigner: username,
       }
     })
      .toPromise()
      .then((tasks) => {
        return tasks;
      })
      .catch((error) => {return null;});
   }

   public deleteTask(assignee: string,
                     assigner: string,
                     description: string): void{
                       this.http.delete(this.taskServiceUrl + '/delete', {
                         params:{
                           assignee: assignee,
                           assigner: assigner,
                           description: description
                         }
                       })
                       .toPromise()
                       .catch((error) => {return null;});
                     }
}
