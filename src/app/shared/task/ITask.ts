export interface ITask {
  id : number;
  assignee: string;
  assigner: string;
  title: string;
  description: string;
}
